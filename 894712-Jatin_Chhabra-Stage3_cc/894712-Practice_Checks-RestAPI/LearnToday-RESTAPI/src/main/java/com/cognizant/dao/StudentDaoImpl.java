package com.cognizant.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.cognizant.models.Course;
import com.cognizant.models.Student;
@Component
public class StudentDaoImpl implements StudentDao{
	@Autowired
	private JdbcTemplate jdbctemplate;
	@Override
	public List<Course> getAllCourses() {
		return jdbctemplate.query("select * from Course order by Start_Date asc", new CourseRowMapper());
	}

	@Override
	public boolean insert(Student student) {
		int res=jdbctemplate.update("insert into Student values(?,?,?)", student.getEnrollmentId(),student.getStudentId(),student.getCourseId());
		if(res>=1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean delete(int EnrollmentId) {
		int res=jdbctemplate.update("delete from Student where EnrollmentId=?",EnrollmentId);
		if(res>=1) {
			return true;
		}
		return false;
	}

	@Override
	public Student getStudentById(int EnrollmentId) {
		PreparedStatementSetter setter =new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setInt(1, EnrollmentId);
			}
		};
		return jdbctemplate.query("select * from Student where EnrollmentId=?", setter, new ResultSetExtractor<Student>() {

			@Override
			public Student extractData(ResultSet rs) throws SQLException, DataAccessException {
				Student st=null;
				if(rs.next()) {
					st=new Student();
					st.setEnrollmentId(rs.getInt(1));
					st.setStudentId(rs.getInt(2));
					st.setCourseId(rs.getInt(3));
				}
				return st;
			}
		});
	}
}
