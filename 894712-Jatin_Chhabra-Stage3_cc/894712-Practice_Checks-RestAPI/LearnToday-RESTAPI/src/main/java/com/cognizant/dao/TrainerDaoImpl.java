package com.cognizant.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.cognizant.models.Trainer;
@Component
public class TrainerDaoImpl implements TrainerDao{
	@Autowired
	private JdbcTemplate jdbctemplate;
	@Override
	public boolean insert(Trainer trainer) {
		int res=jdbctemplate.update("insert into trainer values(?,?)",trainer.getTrainerId(),trainer.getPassword());
		if(res>=1) {
			return true;
		}
		return false;
	}

	@Override
	public boolean update(Trainer trainer) {
		int res=jdbctemplate.update("update trainer set Password=? where TrainerId=?",trainer.getPassword(),trainer.getTrainerId());
		if(res>=1) {
			return true;
		}
		return false;
	}

	@Override
	public Trainer find(int TrainerId) {
		PreparedStatementSetter setter =new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setInt(1, TrainerId);
			}
		};
		return jdbctemplate.query("select * from trainer where TrainerId=?", setter, new ResultSetExtractor<Trainer>() {

			@Override
			public Trainer extractData(ResultSet rs) throws SQLException, DataAccessException {
				Trainer tr=null;
				if(rs.next()) {
					tr=new Trainer();
					tr.setTrainerId(rs.getInt(1));
					tr.setPassword(rs.getString(2));
				}
				return tr;
			}
		});
	}
	
}
